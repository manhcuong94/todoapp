using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VueJS.Models
{
    public partial class TodoItem
    {
        public Guid Id { get; set; }
        public bool? IsDone { get; set; }
        public string Title { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
    }
}
