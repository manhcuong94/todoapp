using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using VueJS.Models;
using VueJS.Services;

namespace VueJS.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/[controller]")]  
    public class TodoController : Controller
    {
        private readonly ITodoItemService _todoItemService;
        

        public TodoController(ITodoItemService todoItemService)
        {
            _todoItemService = todoItemService;

        }

        [Route("GetListIncompleteItem")]
        [HttpGet]
        public async Task<IList<TodoItem>> GetListIncompleteItem()
        {
            var items = await _todoItemService.GetIncompleteItemAsync();

            return items;
        }

        [Route("GetAllItems")]
        [HttpGet]
        public async Task<IList<TodoItem>> GetAllItems()
        {
            var items = await _todoItemService.GetAllItemsAsync();

            return items;
        }

        [Route("AddItem")]
        //[ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> AddItem([FromBody] TodoItemViewBinding newItem)
        {
            if (!ModelState.IsValid || String.IsNullOrEmpty(newItem.Title))
            {
                return BadRequest();
            }
            var item = new TodoItem()
            {
                Title = newItem.Title
            };
            var successful = await _todoItemService.AddItemAsync(item);
            if (!successful)
            {
                return BadRequest();
            }
            return Ok(item);
        }

        [Route("DeleteItem")]
        //[ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> DeleteItem([FromBody] TodoItemViewBinding todo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var itemId = Guid.Empty;
            if(Guid.TryParse(todo.Id, out itemId))
            {
                var item = new TodoItem()
                {
                    Id = itemId
                };
                var successful = await _todoItemService.DeleteItemAsync(item);
                if (!successful)
                {
                    return BadRequest();
                }
                var items = await _todoItemService.GetAllItemsAsync();
                return Ok(items);
            }
            else
            {
                return BadRequest();
            }            
        }

        [Route("MarkDone")]
        //[ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> MarkDone([FromBody] TodoItemViewBinding todo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var itemId = Guid.Empty;
            if (Guid.TryParse(todo.Id, out itemId))
            {
                var item = new TodoItem()
                {
                    Id = itemId
                };
                var successful = await _todoItemService.MarkDoneAsync(itemId);
                if (!successful)
                {
                    return BadRequest("Could not mark item as done.");
                }
                var items = await _todoItemService.GetAllItemsAsync();
                return Ok(items);
            }
            else
            {
                return BadRequest();
            }           
            
        }
    }

    
}
