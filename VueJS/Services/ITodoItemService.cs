using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VueJS.Models;

namespace VueJS.Services
{
    public interface ITodoItemService
    {
        Task<IList<TodoItem>> GetIncompleteItemAsync();
        Task<bool> AddItemAsync(TodoItem newItem);
        Task<bool> MarkDoneAsync(Guid id);
        Task<bool> DeleteItemAsync(TodoItem newItem);
        Task<IList<TodoItem>> GetAllItemsAsync();
        
    }
}
