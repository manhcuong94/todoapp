using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VueJS.Models;

namespace VueJS.Services
{
    public class TodoItemService : ITodoItemService
    {
        private readonly TodoDbContext _todoDbContext;
        public TodoItemService(TodoDbContext todoDbContext)
        {
            _todoDbContext = todoDbContext;
        }

        public async Task<bool> AddItemAsync(TodoItem newItem)
        {
            try
            {
                newItem.Id = Guid.NewGuid();
                newItem.Title = newItem.Title;
                newItem.IsDone = false;
                newItem.CreatedDate = DateTimeOffset.UtcNow;

                _todoDbContext.TodoItem.Add(newItem);

                var saveResult = await _todoDbContext.SaveChangesAsync();
                return saveResult == 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public async Task<bool> DeleteItemAsync(TodoItem item)
        {
            try
            {
                var todo = _todoDbContext.FindAsync<TodoItem>(item.Id);
                if(todo == null)
                {
                    return false;
                }
                _todoDbContext.TodoItem.Remove(todo.Result);
                var saveResult = await _todoDbContext.SaveChangesAsync();
                return saveResult == 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

        public async Task<IList<TodoItem>> GetIncompleteItemAsync()
        {
            var items = await _todoDbContext.TodoItem.Where(x => x.IsDone == false).OrderByDescending(x => x.CreatedDate).ToListAsync();

            return items;
        }

        public async Task<IList<TodoItem>> GetAllItemsAsync()
        {
            var items = _todoDbContext.TodoItem.OrderByDescending(x => x.CreatedDate);

            return await Task.FromResult(items.ToList());
        }

        public async Task<bool> MarkDoneAsync(Guid id)
        {
            try
            {
                var item = await _todoDbContext.FindAsync<TodoItem>(id);
                if (item == null)
                    return false;
                if (item.IsDone == true)
                {
                    item.IsDone = false;
                }
                else
                {
                    item.IsDone = true;
                }
                
                var saveResult = await _todoDbContext.SaveChangesAsync();
                return saveResult == 1; // One entity should have been updated
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
    }
}
