using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using VueJS.Models;
using VueJS.Services;

namespace VueJS.Test
{
    [TestClass]
    public class UnitTest_TodoItemService
    {
        [TestMethod]
        public void Test_GetAllItemsAsync_ReturnList()
        {
            //Arrange
            //var mockSet = new Mock<DbSet<TodoItem>>();
            var mock = new Mock<TodoDbContext>();

            var todoimtems = new List<TodoItem>
            {
                new TodoItem{Id = Guid.NewGuid(), IsDone = true, CreatedDate = DateTime.Now, Title = ""}
            };

            mock.Setup(mc => mc.TodoItem).ReturnsDbSet(todoimtems);

            var service = new TodoItemService(mock.Object);
            var result =  service.GetAllItemsAsync().Result;
            Assert.AreEqual(1, result.Count);
        }
    }
}
